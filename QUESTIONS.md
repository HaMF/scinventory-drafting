# (Not so) frequently asked questions


## Persistence of DOIs and physical labels (QR codes/bar codes)

A DOI is in the whitepaper considered to be the digital identifier of a physical object. It has in that sense meaning only as long as the object it referes to exists. Scientific samples as such are often indistinguishable by their apperance. Thus, loss of the label (e.g. QR-code) on the sample box is equivalent to the sample being lost. (In this case, the DOI can be viewed as a reference to the digital information stored about the sample. This also holds for a label without attached sample alike. The persistance of a printed label (for example a QR-code) is therefore the same scope as a DOI.)

## Granularity of information

The system is not supposed to make any restrictions on the "granularity" of the information. Wether a batch of samples gets one DOI assigned or every single sample of the batch gets an individual DOI lies in the discression of the scientist. This decision can be based on established best practices in the institute, readability and other factors.  The (arbitrarly) large address space whithin the institutes prefix does not call for a limit as such.