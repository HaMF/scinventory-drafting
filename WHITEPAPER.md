# scinventory

An easy to use and reliable scientific inventory management system can help scientists track samples from production to publication (and beyond!). The concept also can be applied to laboratory equipment and measurement devices.

This document only describes the most simple concept and use case. The source code of an implementation needs to be made open source under a free license and contributions shall be actively encouraged.

## General remarks

>The following examples comes from the field of solid state physics but the concepts are in fact a more general ones that also works for other natural sciences.

A **database of samples** in an research institute will be created that contains semantic data and can such be **easily searched and filtered.** Researchers need to see an immediate benefit over their traditional lab book and home-build solutions (Excel sheets) so that they adapt the system. Therefore **ease of use** will be a top priority. As different research institutes have different needs, the data scheme will evolve over time. Utilizing self learning algorithms (in some way like the "other customers bought" suggestions at Amazon) it will be made especially easy for the researchers to enter information in the way "everyone does it". This way, the structure and searchability of the database can be **maintained (semi)automatically**. The system will **promote collaborating** with other institutes by allowing to selectively share information easily with a third person.

The interface will be **completely browser based** and work on a desktop computer, tablet or smart phone alike. Thus leveraging the fact that virtually everyone carries a smart phone in his pocket and most equipment has dedicated control computers. Every sample will be **identified with a DOI-like identifier** that is noted on the physical sample in the form of an **QR-code, NFC tag** that allows quick access to the samples metadata. **Publishing the data** and therefore registering the internal as a public DOI is encouraged and easy in the interface. For resolving the DOI, the exact same method as used for the internal DOI-like identifiers is used.


### Creating samples

>Samples in this context are for example small (5x5x1mm³) crystals that are created, diced into multiple pieces and processed. 

> Typically a series of samples is created for which the creation process varys only slightly. Finally, measurements on a sample or a series of samples lead to a publication. Samples are then stored and may be reused for other measurements and other publications.

When a sample is created, the detailed process parameters are today noted in either a laboratory notebook or the personal notebook of the scientist (or both). This is the scientific "best practice" to generate reproducible results. The sample is typically labelled with a "unique" name. Later, the parameters are often copied into a home-build "database" (excel sheets or similar) to keep an overview. This step is often delayed, however, which leads sometimes to two samples with the same name generating a lot of confusion.

  * At creation the sample gets an internal DOI-like identifier and is labelled with the appropriate QR-code.
  * Scanning the code (or entering the identifier in the interface) immediately (after authentication) opens the scinventory entry mask for process parameters of a new sample. The sample can be given a pronounceable "nick-name" at that time.
  * Instead of noting the parameters at three different locations they can be directly entered into scinventory once. 
  * The so-entered (meta)data are then immediately accessible from everywhere in the institute and can be shared privately with collaborators.
  * Parameters from computer controlled processes can be automatically associated with a sample.

Multiple researchers in the institute might work on the same sample. In order to know how the sample was treated and what measurements have been performed, today typically multiple computers and lab books need to be checked.

  * Attaching all measurement data (and processing steps) of the sample in scinventory at the time of the measurement allows to track the history and all work done on the sample.
  * Computer controlled measurements can be automatically associated with the sample. Routine measurements can be automatically analyzed!
  * Processing steps may be accompanied by a new DOI. Dicing a sample into two halves is the prime example for this.
  * The original dataset stays in the database.
  * The original ("parent") sample is referenced in the derived ("child") sample and vice versa.

Once new insight is gained from (a series of) samples and a publication with the data is written, the data and all connected meta-data can be easily "frozen" to the current status by the click of a button. A permanent public DOI can be assigned to this state and the data can be published and referenced in the main publication. Associated samples (parents) will be published automatically at the same time. If this is not wished, the parents data can be selectively merged with the data of the published child. Either way is is ensured that no not-registered DOIs are referenced and that the full relevant history is available alongside the data.

## Contact

Written 2015-10-21 by Hannes Maier-Flaig (hannes.maier-flaig@wmi.badw.de). More details are available in QUESTIONS.md

Feel free to get in touch! 